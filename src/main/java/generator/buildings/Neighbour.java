package generator.buildings;

public enum Neighbour{
    n1(-1,-1),
    n2(0,-1),
    n3(1,-1),
    n4(-1,0),
    n5(1,0),
    n6(-1,1),
    n7(0,1),
    n8(1,1);
    int x;
    int y;
    private Neighbour(int x, int y){
        this.x = x;
        this.y = y;
    }
}
