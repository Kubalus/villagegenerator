package generator.buildings;

import generator.SpaceMatrix;
import generator.buildings.model.Center;
import generator.buildings.model.Plot;
import generator.curves.Point;
import generator.curves.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static generator.curves.Utils.fill;


public class CenterGenerator {
    private int size;
    private SpaceMatrix spaceMatrix;
    private double sizeParam;
    private int plotsNumber;
    private Random random;
    // sizeX to wymiar przyciętej mapy w osi X a sizeY w osi Y.
    private int maxX,minX,maxY,minY, sizeX, sizeY;

    // TODO: Poprowadzenie drogi z punktu centerPoint do drogi głównej
    private Point centerPoint;

    // TODO: dodaj parametry z serwera sizeParam i plotsNumber;
    public CenterGenerator(int size, SpaceMatrix sp, int plNum){
        this.random = new Random();
        this.size = size;
        this.spaceMatrix = sp;
        this.plotsNumber = plNum;
    }

    public List<Plot> generateCenter(){
        boolean[][] centerMask = this.cutToCircle(this.spaceMatrix.greatestSpace());
        List<Plot> plots = generatePlots(centerMask);
        divideSpaceBetweenPlots(plots,centerMask);
       return plots;
    }

    public List<Plot> generatePlots(boolean[][] mask){
        List<Plot> plots = new ArrayList<>();
        boolean[][] maskClone = mask.clone();
        int x,y;
        for (int i = 0; i < plotsNumber; i++) {
            x = random.nextInt(sizeX) + minX;
            y = random.nextInt(sizeY) + minY;
            while(maskClone[x][y]){
                x = random.nextInt(sizeX) + minX;
                y = random.nextInt(sizeY) + minY;
            }
            maskClone[x][y] = false;
            plots.add(new Plot(x,y));
        }
        return plots;
    }

    // TODO: Przerobienie brute Force'a na coś bardziej ludzkiego :D
    private void divideSpaceBetweenPlots(List<Plot> plots, boolean[][] mask) {
        double maxDist = Math.max(sizeX * 2, sizeY * 2);
        double minDist, dist;
        Plot nearestPlot;
        for (int i = minX; i <= this.maxX; i++) {
            for (int j = minY; j <= this.maxY; j++) {
                if (mask[i][j]){
                    minDist = maxDist;
                    nearestPlot = null;//plots.get(0);
                    for (Plot plot: plots) {
                        dist = distance(i,j,plot.getX(),plot.getY());
                        if(dist < minDist){
                            minDist = dist;
                            nearestPlot = plot;
                        }
                    }
                    this.spaceMatrix.setPlace(i,j, SpaceMatrix.PlaceType.Building);
                    nearestPlot.addPoint(new Point(i,j));
                }
            }
        }

        for (Plot plot: plots) {
            plot.makeMap(this.spaceMatrix.height);
            plot.updateSpaceMatrix(this.spaceMatrix);
            plot.cutToBorder();
        }

    }


    private boolean[][] cutToCircle( boolean[][] mask){
        cutUsedSpace(mask);
        boolean[][] circleMask = new boolean[mask.length][mask[0].length];
        double sumX = 0,sumY = 0;
        int counter = 0;
        boolean started = false;

        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                if(mask[i][j])
                    started = true;
                if(started){
                    sumX += i;
                    sumY += j;
                    counter++;
                    j += 2;
                }
            }
            if(started)
                i += 2;
        }

        int pointX = (int) sumX / counter;
        int pointY = (int) sumY / counter;
        this.centerPoint = new Point(pointX,pointY);
        int currentX = pointX, currentY = pointY;
        int range = 1;
        double distance = -1, tempDistance;
        while(distance == -1){
            if(range % 2 == 1){
                for(int i = 0; i < range; i++){
                    if(currentX > 0) {
                        currentX--;
                        if (!mask[currentX][currentY] || currentX == 0) {
                            tempDistance = distance(currentX, currentY, pointX, pointY);
                            if (distance == -1 || tempDistance < distance) {
                                distance = tempDistance;
                            }
                        }
                    }
                }
                for(int i = 0; i < range; i++){
                    if(currentY > 0) {
                        currentY--;
                        if (!mask[currentX][currentY] || currentY == 0) {
                            tempDistance = distance(currentX, currentY, pointX, pointY);
                            if (distance == -1 || tempDistance < distance) {
                                distance = tempDistance;
                            }
                        }
                    }
                }
            }
            if(range % 2 == 0){
                for(int i = 0; i < range; i++){
                    if((currentX < this.size - 1)) {
                        currentX++;
                        if (!mask[currentX][currentY] || currentX == this.size - 1) {
                            tempDistance = distance(currentX, currentY, pointX, pointY);
                            if (distance == -1 || tempDistance < distance) {
                                distance = tempDistance;
                            }
                        }
                    }
                }
                for(int i = 0; i < range; i++){
                    if(currentY < this.size - 1) {
                        currentY++;
                        if (!mask[currentX][currentY] || currentY == this.size - 1) {
                            tempDistance = distance(currentX, currentY, pointX, pointY);
                            if (distance == -1 || tempDistance < distance) {
                                distance = tempDistance;
                            }
                        }
                    }
                }
            }
            range ++;
        }
        distance = 0.7 * distance;
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                if(distance(pointX,pointY,i,j) <= distance)
                    circleMask[i][j] = true;
                else
                    circleMask[i][j] = false;
            }
        }


        return circleMask;
    }


    private double distance(int x1, int y1, int x2, int y2){
        int x = x1 - x2;
        int y = y1 - y2;
        return Math.sqrt(x * x + y * y);
    }

    private void cutUsedSpace(boolean[][] mask) {
        this.maxX = 0;
        this.maxY = 0;
        this.minX = mask.length;
        this.minY = mask[0].length;
        for (int i = 0; i < mask.length; i++) {
            for (int j = 0; j < mask[0].length; j++) {
                if (mask[i][j]) {
                    if (i > maxX)
                        maxX = i;
                    if (j > maxY)
                        maxY = j;
                    if (i < minX)
                        minX = i;
                    if (j < minY)
                        minY = j;
                }
            }
        }
        this.sizeX = maxX - minX;
        this.sizeY = maxY - minY;
    }

}
