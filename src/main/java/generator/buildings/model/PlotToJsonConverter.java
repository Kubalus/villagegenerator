package generator.buildings.model;

import org.json.JSONObject;

import generator.curves.Point;
import java.util.LinkedList;
import java.util.List;

public class PlotToJsonConverter {
    public static final String id = "building";

    public JSONObject converte(Plot plot) {
         boolean[][] workBorder = plot.border.clone();

         List<Point> points = getCycle(workBorder);
         //reduceLines(points);
         return generateJson(points);
    }

    private List<Point> getCycle(boolean[][] border) {
        int currentX = 0;
        int currentY = 0;
        for (int i = 0; i < border.length; i++) {
             for (int j = 0; j < border[0].length; j++) {
                 if (border[i][j]) {
                     currentX = i;
                     currentY = j;
                     break;
                 }
             }
        }

        List<Point> points =  new LinkedList();
        points.add(new Point(currentX, currentY));
        border[currentX][currentY]  = false;

        boolean changed = false;
        do {
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (isUnvisitedBorder(border, currentX + i, currentY + j)) {
                        currentX += i;
                        currentY += j;
                        border[currentX][currentY] = false;
                        points.add(new Point(currentX, currentY));
                        changed = true;
                        break;
                    }
                }
                if (changed) {
                    break;
                }
            }
        } while (changed);

        return points;
    }

    private boolean isUnvisitedBorder(boolean[][] border, int x, int y) {
        if (x >= 0 && x < border.length) {
            if (y >= 0 && y < border[0].length) {
                if (border[x][y]) {
                    return true;
                }
            }
        }

        return false;
    }

    private JSONObject generateJson(List<Point> points) {
        JSONObject js  = new JSONObject();
        js.put("id", id);
        js.put("points", points);
        return js;
    }



}
