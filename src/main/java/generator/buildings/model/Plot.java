package generator.buildings.model;

import generator.SpaceMatrix;
import generator.curves.Point;
import generator.curves.Utils;
import server.Villager;

import java.util.ArrayList;
import java.util.List;

public class Plot {
    int x,y;
    boolean[][] map;
    boolean[][] border;
    List<Point> points;
    Villager owner;


    public Plot(int x, int y){
        this.x = x;
        this.y = y;
        points = new ArrayList<>();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void addPoint(Point point){
        this.points.add(point);
    }

    public void makeMap(int size){
        this.map = Utils.fill(new boolean[size][size], false);
        for (Point point: this.points) {
            this.map[point.getX()][point.getY()] = true;
        }
    }

    public void updateSpaceMatrix(SpaceMatrix sm){
        for (Point point: this.points) {
            sm.setPlace(point.getX(),point.getY(), SpaceMatrix.PlaceType.Building);
        }
    }

    public void cutToBorder(){
        int size = map.length;
        this.border =  Utils.fill(new boolean[map.length][map[0].length], false);
        boolean flag;
        for (int i = 0; i < size ; i++) {
            flag = false;
            for (int j = 0; j < size; j++) {
                if(!flag && map[i][j]){
                    flag = true;
                    border[i][j] = true;
                }
                if(map[i][j] &&  (j < size -1)){
                    if(!map[i][j+1])
                        border[i][j] = true;
                }

            }

        }
        for (int j = 0; j < size; j++){
            flag = false;
            for (int i = 0; i < size ; i++)  {
                if(!flag && map[i][j]){
                    flag = true;
                    border[i][j] = true;
                }
                if(map[i][j] &&  (i < size -1)){
                    if(!map[i+1][j])
                        border[i][j] = true;
                }

            }

        }
    }
}
