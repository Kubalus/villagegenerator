package generator;

import generator.curves.Point;
import generator.curves.graphs.Vertex;

import java.util.List;
import java.util.Stack;

import static generator.curves.Utils.fill;

public class SpaceMatrix {
    public final int width;
    public final int height;

    private final PlaceType[][] matrix;
    public enum PlaceType {
        Free("_"), Road("X"), Building("+"),River("~"),Bridge("B"), Colision("C");

        private final String symbol;

        PlaceType(String symbol){
            this.symbol = symbol;
        }

        public String getSymbol(){
            return this.symbol;
        }

    }

    public SpaceMatrix(int width, int height) {
        matrix = new PlaceType[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                matrix[i][j] = PlaceType.Free;
            }
        }
        this.width = width;
        this.height = height;
    }

    public void setPlace(int x, int y, PlaceType type) {
        matrix[x][y] = type;
    }

    public PlaceType getPlace(int x, int y) {
        return matrix[x][y];
    }

    public void insertRoad(List<Vertex> road){
        for(Vertex vertex: road){
            //this.setPlace(vertex.getPos_x(),vertex.getPos_y(), PlaceType.Road);
        }
    }

    public boolean[][] greatestSpace(){
        int size, maxSize = -1;
        int sizeX = -1 , sizeY = -1;
        boolean[][] used = fill(new boolean[this.width][this.height], false);
        for(int i = 0; i < this.width; i++){
            for (int j = 0; j < this.height; j++){
                if(!(matrix[i][j] == PlaceType.Free)) {
                    used[i][j] = true;
                    continue;
                }
                if(!used[i][j]){
                    size = countSpace(i, j, used, toBoolean(matrix));
                    if(size > maxSize){
                        maxSize = size;
                        sizeX = i;
                        sizeY = j;
                    }
                }
            }
        }

        return createMask(new Point(sizeX,sizeY,width,height), toBoolean(matrix));
    }

    public static boolean[][] createMask(Point start,boolean[][] borders){

        Stack<Point> stack = new Stack<>();
        stack.push(start);
        Point current;
        boolean[][] map = new boolean[borders.length][borders[0].length];
        map[start.getX()][start.getY()] = true;
        List<Point> points;
        while(!stack.empty()){
            current =stack.pop();
            points = current.neighbors();
            for(Point point: points){
                if(!(borders[point.getX()][point.getY()])) {
                    continue;
                }
                if(!map[point.getX()][point.getY()]){
                    map[point.getX()][point.getY()] = true;
                    stack.push(point);
                }

            }
        }
        return map;
    }

    private static int countSpace(int x, int y, boolean[][] map, boolean[][] border){

        Stack<Point> stack = new Stack<>();
        stack.push(new Point(x,y,map.length, map[0].length));
        Point current;
        map[x][y] = true;
        int size = 0;
        List<Point> points;
        while(!stack.empty()){
            current = stack.pop();
            size++;
            points = current.neighbors();
            for (Point point: points) {

                if(!(border[point.getX()][point.getY()])) {
                    map[point.getX()][point.getY()] = true;
                    continue;
                }
                if(!map[point.getX()][point.getY()]){
                    map[point.getX()][point.getY()] = true;
                    stack.push(point);
                }
            }
        }

        return size;
    }

    public static boolean[][] toBoolean(PlaceType[][] map){
        boolean[][] result = new boolean[map.length][map[0].length];
        for(int i = 0; i < map.length; i++){
            for (int j = 0; j < map[0].length; j++) {
                if(map[i][j] == PlaceType.Free)
                    result[i][j] = true;
            }
        }
        return result;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < this.height; i ++){
            for (int j = 0; j < this.width ; j++) {
                result.append(this.getPlace(i, j).getSymbol()).append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }
}
