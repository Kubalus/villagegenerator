package generator;

import generator.buildings.CenterGenerator;
import generator.buildings.model.Plot;
import generator.buildings.model.PlotToJsonConverter;
import generator.curves.Curve;
import generator.curves.CurvesGenerator;
import generator.curves.Road;
import generator.curves.River;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class MainGenerator {
    int size;
    CurvesGenerator curvesGenerator;


    public Village generateVillage(GenerationParameters params){
        this.size = params.size; // należy ustawić rozmiaz zgodnie z parametrami

//        size = 20;
        SpaceMatrix spaceMatrix = new SpaceMatrix(size, size);
        Village vil = new Village();

        generateRoads(vil, spaceMatrix, params.roadType);
        if(params.river)
            addRiver(vil, spaceMatrix);
        addCenter(vil, spaceMatrix);

	System.out.println(spaceMatrix.toString());


        return vil;
    }

    private void generateRoads(Village vil, SpaceMatrix sp, GenerationParameters.RoadType roadType){
        CurvesGenerator curvesGenerator =  new CurvesGenerator(size, sp);
        vil.addRoad(curvesGenerator.generateRoad(roadType));
    }

    private void addRiver(Village vil,SpaceMatrix sp){
        CurvesGenerator curvesGenerator =  new CurvesGenerator(size, sp);
        vil.addRiver(curvesGenerator.generateRiver());
    }


    private void addCenter(Village vil, SpaceMatrix sp){
        CenterGenerator centerGenerator = new CenterGenerator(this.size, sp, 50);
        vil.setCenter(centerGenerator.generateCenter());
    }

    public class Village {
        List<Road> roads = new LinkedList<>();

        private List<Plot> center;
        List<River> rivers = new LinkedList<>();


        void addRoad(Road r) {
            roads.add(r);
        }


        public void setCenter(List<Plot> plots) {
            center = plots;
        }

        void addRiver(River r) {
            rivers.add(r);

        }

        public String getJsonDescription(){
            JSONArray array = new JSONArray();
            for (Road road : roads) {
                //System.out.println(Curve.getJsonDescription(road) + " gener");
                array.put(new JSONObject(Curve.getJsonDescription(road)));
            }


            PlotToJsonConverter plotConverter = new PlotToJsonConverter();
            for (Plot pl : center) {

                array.put(plotConverter.converte(pl));
                System.out.println(array.toString());
            }

            for (River river : rivers) {
                //System.out.println(Curve.getJsonDescription(river) + " gener");
                array.put(new JSONObject(River.getJsonDescription(river)));

            }

            return array.toString();
        }
    }

}
