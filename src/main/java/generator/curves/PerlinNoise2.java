package generator.curves;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class PerlinNoise2 {
    private Random random;
    private int size;
    private double[][] matrix;

    public PerlinNoise2(int size, int precision){
        this.size = size;
        matrix = new double[size][size];
        for(int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = 0;
            }
        }
        double[][][] data =  new double[precision][size][size];
        random = new Random();
        for(int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                data[1][i][j] = Utils.booleanToInt(random.nextBoolean());
            }
        }
        for(int i = 2; i < precision ; i ++){
            data[i] = interpolate(data[1], i, size);
        }

        double weight = 1.0;
        for(int i = precision - 1; i > 0; i--){
            weight /= 2;
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    matrix[j][k] += data[i][j][k] * weight;
                }

            }
        }

    }

    public double[][] getMatrix() {
        return matrix;
    }

    private double[][] interpolate(double[][] matrix, int div, int size){
        int bound = size / div;
        double[][] small = new double[bound + div][bound + div];
        double[][] result = new double[size][size];
        for (int i = 0; i < bound + div ; i++) {
            for (int j = 0; j < bound + div; j++) {
                small[i][j] = matrix[i][j];
            }
        }
        double val1, val2,val3,val4;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if(i % div == 0 && j % div == 0){
                    result[i][j] = small[i / div][j / div];
                }
                else if( i % div == 0){
                    val1 = small[i / div][j / div];
                    val2 = small[i / div][(j / div) + 1];
                    result[i][j] = (val1 * (div - j % div) + val2 * (j % div)) / div;
                }
                else if( j % div == 0){
                    val1 = small[i / div][j / div];
                    val2 = small[(i / div) + 1][j / div];
                    result[i][j] = (val1 * (div - i % div) + val2 * (i % div)) / div;
                }
                else{
                    val1 = small[i / div][j / div];
                    val2 = small[i / div][(j / div) + 1];
                    val3 = small[(i / div) + 1][j / div];
                    val4 = small[(i / div) + 1][j / div + 1];
                    double temp1 = (val1 * (div - j % div) + val2 * (j % div)) / div;
                    double temp2 = (val3 * (div - j % div) + val4 * (j % div)) / div;
                    result[i][j] = (temp1 * (div - i % div) + temp2 * (i % div)) / div;
                }
            }
        }
        return result;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < size; i ++){
            for (int j = 0; j < size ; j++) {
//                result.append(Utils.round((float)matrix[i][j],3) + " ");
                result.append((int)(matrix[i][j] * 256)+  " ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    private void createBMP(){
        BufferedImage img = new BufferedImage(size,size, BufferedImage.TYPE_INT_RGB);
        File f = null;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int a = (int) (matrix[i][j] * 256);
                int p = (a<<16) | (a<<8) | a;
                img.setRGB(i,j,p);

            }

        }
        try {
            f = new File("src\\main\\resources\\out.png");
            ImageIO.write(img,"png",f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveToFile(){
        String result = this.toString();
        try {
            FileWriter fw = new FileWriter("src\\main\\resources\\noise.txt");
            fw.write(result);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadFromFile(){

    }

    public static void main(String[] args){
       PerlinNoise2 pNoise = new PerlinNoise2(1200,40);
       pNoise.createBMP();
       pNoise.saveToFile();
    }

}
