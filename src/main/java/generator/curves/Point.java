package generator.curves;

import java.util.ArrayList;
import java.util.List;

public class Point {
    int x;
    int y;
    int maxX; // size of array: for [1][1] equals 1
    int maxY;

    public Point(int x, int y, int maxX, int maxY){
        this.x = x;
        this.y = y;
        this.maxX = maxX;
        this.maxY = maxY;
    }

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public List<Point> neighbors(){
        List<Point> points = new ArrayList<>();
        if(this.x > 0)
            points.add(new Point(this.x-1, this.y, this.maxX, this.maxY));
        if(this.x < (this.maxX - 1))
            points.add(new Point(this.x+1, this.y, this.maxX, this.maxY));
        if(this.y > 0)
            points.add(new Point(this.x, this.y-1, this.maxX, this.maxY));
        if(this.y < (this.maxY - 1))
            points.add(new Point(this.x, this.y+1, this.maxX, this.maxY));
        return points;
    }
}
