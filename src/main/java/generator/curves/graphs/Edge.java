package generator.curves.graphs;

import generator.curves.graphs.Vertex;

public class Edge {
    private Vertex begin;
    private Vertex end;
    private double weight;

    Edge(Vertex begin, Vertex end, double weight){
        this.begin = begin;
        this.end = end;
        this.weight = weight;
    }

    public Vertex getBegin() {
        return begin;
    }

    public Vertex getEnd() {
        return end;
    }

    public double getWeight() {
        return weight;
    }
}
