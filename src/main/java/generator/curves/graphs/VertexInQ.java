package generator.curves.graphs;

public class VertexInQ implements Comparable<VertexInQ> {
    private Vertex vertex;
    private int heuristic;
    private double cost;

    public VertexInQ(Vertex vertex, int heuristic, double cost) {
        this.vertex = vertex;
        this.heuristic = heuristic;
        this.cost = cost;
    }


    public int compareTo(VertexInQ vertex) {
        if(this.getHeuristic() == vertex.getHeuristic()) {
            double cost = this.getCost() - vertex.getCost();
            if (cost > 0)
                return 1;
            else
                return -1;
        }
        else
            return this.getHeuristic() - vertex.getHeuristic();
    }

    public Vertex getVertex() {
        return vertex;
    }

    public int getHeuristic() {
        return heuristic;
    }

    public double getCost() {
        return cost;
    }
}





