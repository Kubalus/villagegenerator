package generator.curves.graphs;

import generator.curves.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

import static java.lang.StrictMath.*;


public class Graph {
    private int size_x;
    private int size_y;
    private Vertex center;
    private List<Vertex> vertexes;

    Graph(int x, int y){
        this.size_x = x;
        this.size_y = y;
        this.vertexes = new ArrayList<>();
        for(int i = 0; i < x; i++){
            for (int j = 0; j < y; j++)
            vertexes.add(new Vertex(i, j));
        }
        this.center = this.getVertex(x / 2, y / 2);
    }
    public Vertex getVertex(int x, int y){

        return vertexes.get(Utils.flattedIndex(size_x, x, y));
    }

    public Vertex getCenter() {
        return center;
    }

    void addEdge(int x1, int y1, int x2, int y2, double weight){
        Vertex begin, end;
        begin = this.getVertex(x1, y1);
        end = this.getVertex(x2, y2);
        Edge edge = new Edge(begin, end, weight);
        begin.addEdge(edge);

        edge = new Edge(end, begin, weight);
        end.addEdge(edge);
    }

    void addEdge(Vertex begin, Vertex end, double weight){
        Edge edge = new Edge(begin, end, weight);
        begin.addEdge(edge);
    }

    int heuristic(Vertex a, Vertex b){
        return Math.abs(a.getPos_x() - b.getPos_x() ) + Math.abs(a.getPos_y() - b.getPos_y() );
    }

    void print (){
        Vertex current;
        for(int i = 0; i < this.size_x; i++){
            for(int j = 0; j < this.size_y; j++){
                current = this.getVertex(i,j);
                System.out.print("(" + current.getValue() + ") ");
            }
            System.out.println();
        }
    }

    
    public void printPath(List<Vertex> path){
        char[][] map = new char[this.size_x][this.size_y];
        for(int i = 0; i < this.size_x; i++) {
            for (int j = 0; j < this.size_y; j++) {
                map[i][j]='_';
            }
        }
        for (Vertex vertex: path) {
            map[vertex.getPos_x()][vertex.getPos_y()] = 'X';
        }
        for(int i = 0; i < this.size_x; i++){
            for(int j = 0; j < this.size_y; j++){
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }

    public List<Vertex> aStarAlgorithm(Vertex start, Vertex target){

        List<Vertex> finalPath = new ArrayList<>();
        PriorityQueue<VertexInQ> priorityQueue = new PriorityQueue<>();
        int size = this.vertexes.size();
        double[] costs = new double[size];
        Vertex[] previous = new Vertex[size];
        Arrays.fill(costs, Integer.MAX_VALUE);
        costs[Utils.flattedIndex(this.size_x,start.getPos_x(),start.getPos_y())] = 0;
        priorityQueue.add(new VertexInQ(start,0,0));

        Vertex current, edgeEnd, next;
        List<Edge> currentEdges;
        double currentCost, edgeCost;
        int edgeIndex;

        while(!priorityQueue.isEmpty()){
            current = priorityQueue.poll().getVertex();

            if(current == target){
                finalPath.add(current);
                while(current != start) {
                    next = previous[Utils.flattedIndex(this.size_x, current.getPos_x(), current.getPos_y())];
                    finalPath.add(next);
                    current = next;
                }
                return finalPath;
            }

            currentEdges = current.getEdges();
            currentCost = costs[Utils.flattedIndex(this.size_x, current.getPos_x(), current.getPos_y())];
            for (Edge edge: currentEdges) {
                edgeEnd = edge.getEnd();
                edgeCost = currentCost + edge.getWeight();
                edgeIndex = Utils.flattedIndex(this.size_x, edgeEnd.getPos_x(), edgeEnd.getPos_y());
                if (costs[edgeIndex] > edgeCost) {
                    costs[edgeIndex] = edgeCost;
                    previous[edgeIndex] = current;
                    priorityQueue.add(new VertexInQ(edgeEnd, heuristic(current,target),edgeCost));
                }
            }
        }
        return null;
    }

    public static Graph graphFromMatrix(double[][] matrix){
        int x = matrix[0].length;
        int y = matrix.length;
        Graph graph = new Graph(x,y);
        for(int i = 0; i < y; i++){
            for(int j = 0; j < x; j++){
                graph.getVertex(j,i).setValue(matrix[j][i]);
                if(i < y-1){
                    graph.addEdge(j,i,j,i+1, graph.calculateWeight(j,i,j,i+1, matrix));
                }
                if(j < x-1){
                    graph.addEdge(j,i,j+1,i, graph.calculateWeight(j,i,j+1,i,matrix));
                }
            }
        }
        return graph;
    }


    private double calculateWeight(int x1, int y1, int x2, int y2, double[][] matrix){
        return exp(0.99 + abs(matrix[x1][y1] = matrix[x2][y2]));
    }

    private double basicWeight(double val1, double val2){
        return pow(1 + val1 - val2, 4);
    }

    private double midDistance(int x1, int y1, int x2, int y2){
        Vertex mid = this.getCenter();
        int xMid = mid.getPos_x();
        int yMid = mid.getPos_y();
        return max(distance(x1,y1,xMid,yMid), distance(x2,y2,xMid,yMid));
    }

    private static double distance(int x1, int y1, int x2, int y2){
        return Math.abs(sqrt(pow(x1 - x2,2) + pow(y1 - y2, 2) ));
    }




}
