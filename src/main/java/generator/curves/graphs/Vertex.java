package generator.curves.graphs;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
    private int pos_x, pos_y;
    private double value;
    List<Edge> edges;

    Vertex(int x, int y){
        edges = new ArrayList<>();
        this.pos_x = x;
        this.pos_y = y;
        value = 0;
    }

    Vertex(int x, int y, double value){
        this(x,y);
        this.value = value;
    }

    @Override
    public String toString() {
        return "(" + pos_x + ", " + pos_y + " )";
    }

    public void addEdge(Edge edge ){
        this.edges.add(edge);
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getPos_y() {
        return pos_y;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    public int getPos_x() {
        return pos_x;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public List<Edge> getEdges(){
        return edges;
    }
}
