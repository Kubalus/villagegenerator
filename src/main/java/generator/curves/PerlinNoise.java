package generator.curves;

import java.util.Random;

import static java.lang.Math.round;


public class PerlinNoise {

    private static Random sRandom = new Random();
    private float[][][] mGradient;
    private int height, width;

    public PerlinNoise(int width, int height) {
        this.width = width;
        this.height = height;
        width++;
        height++;
        mGradient = new float[height][width][2];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                mGradient[i][j][0] = (float) (sRandom.nextDouble() * 2 -1);
                if (sRandom.nextDouble() > 0.5) {
                    mGradient[i][j][1] = (float) Math.sqrt(1 - mGradient[i][j][0] * mGradient[i][j][0]);
                } else {
                    mGradient[i][j][1] = (float) -Math.sqrt(1 - mGradient[i][j][0] * mGradient[i][j][0]);
                }
            }
         }
     }

    /**Function returns value of Perlin Noise at given position.*/
    public float getNoiseAt(float x, float y) {
        int floorX = (int) Math.floor(x);
        int floorY = (int) Math.floor(y);

        float product0 = computeDotProduct(floorX, floorY, x, y);
        int floorX1 = floorX + 1;
        float product1 = computeDotProduct(floorX1, floorY, x, y);
        float mantissaX = x - floorX;
        float ix0 = interpolateLinear(product0, product1, mantissaX);

        int floorY1 = floorY + 1;
        product0 = computeDotProduct(floorX, floorY1, x, y);
        product1 = computeDotProduct(floorX1, floorY1, x, y);
        float ix1 = interpolateLinear(product0, product1, mantissaX);

        return interpolateLinear(ix0, ix1, y - floorY);
    }

    private float computeDotProduct(int iX, int iY, float fX, float fY) {
        return (fX - iX) * mGradient[iY][iX][0] + (fY -iY) * mGradient[iY][iX][1];
    }

    private float interpolateLinear(float a0, float a1, float weight) {
        return (1.0f - weight) * a0 + weight * a1;
    }

    public double[][] toMatrix() {
        double[][] matrix = new double[this.width][this.height];
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                matrix[i][j] = this.getNoiseAt((float)(i + 0.5), (float)(j + 0.5));
            }
        }
        return matrix;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < this.height; i ++){
           for (int j = 0; j < this.width ; j++) {
               result.append(Utils.round(this.getNoiseAt((float)(i + 0.5),(float)( j + 0.5)),2)).append(" ");
           }
           result.append("\n");
        }
        return result.toString();
    }

    public static void main(String[] args){
        System.out.println(3/2);
//        System.out.println(new PerlinNoise(20,20).toString());
    }

}
