package generator.curves;

import java.util.List;

import generator.SpaceMatrix;
import generator.curves.graphs.Vertex;

public class Road extends Curve {
    private String id = "Road";
    public Road(List<Vertex> path, int width, SpaceMatrix spaceMatrix) {
        super(path, width, spaceMatrix);
        System.out.println(spaceMatrix.toString());
    }

    protected void occupyRectangle(int xStart, int yStart, int xEnd, int yEnd,
                                   SpaceMatrix spaceMatrix) {
        xStart = Math.max(0, xStart);
        yStart = Math.max(0, yStart);
        xEnd = Math.min(spaceMatrix.width - 1, xEnd);
        yEnd = Math.min(spaceMatrix.height - 1, yEnd);

        for (int i = xStart; i <= xEnd; i++) {
            for (int j = yStart; j <= yEnd; j++) {
                spaceMatrix.setPlace(i, j, SpaceMatrix.PlaceType.Road);
            }
        }
    }
}
