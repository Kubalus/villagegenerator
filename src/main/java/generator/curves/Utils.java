package generator.curves;

import java.io.FileDescriptor;
import java.math.BigDecimal;

public class Utils {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED = "\u001B[31m";


    // by JB
    public static <T extends Comparable<T>> boolean between(T lower, T compared, T upper) {
        return compared.compareTo(lower) > 0 && upper.compareTo(compared) > 0;
    }

    public static int flattedIndex(int size_x, int x, int y) {
        return x * size_x + y;
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static String matrixToString(boolean[][] matrix) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j])
                    result.append(ANSI_GREEN + "0" + ANSI_RESET);
                else
                    result.append(ANSI_RED + "X" + ANSI_RESET);
                result.append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    public static int booleanToInt(boolean val) {
        return val ? 1 : 0;
    }


    public static Double[][] fill(Double[][] array, Double value) {
        Double[][] result = new Double[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                result[i][j] = value;
            }

        }
        return result;
    }

    public static boolean[][] fill(boolean[][] array, boolean value) {
        boolean[][] result = new boolean[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                result[i][j] = value;
            }

        }
        return result;
    }
}
