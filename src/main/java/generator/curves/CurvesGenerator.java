package generator.curves;

import generator.GenerationParameters;
import generator.MainGenerator;
import generator.curves.graphs.Graph;
import generator.curves.graphs.Vertex;
import generator.SpaceMatrix;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

public class CurvesGenerator {
    int size;
    SpaceMatrix space;
    Random rand;

    public CurvesGenerator(int x, SpaceMatrix space){
        this.size = x;
        this.space = space;
        this.rand = new Random();

    }

    public Road generateRoad(GenerationParameters.RoadType roadType){
        Graph graph = prepareGraph();
        int[] curveParams = generateCoordinates();
        List<Vertex> path = graph.aStarAlgorithm(graph.getVertex(curveParams[0],curveParams[1]),
                graph.getVertex(curveParams[2],curveParams[3]));
        switch (roadType){
            case FORKED:
                curveParams = generateCoordinate();
                path.addAll(graph.aStarAlgorithm(path.get(rand.nextInt(path.size())),
                        graph.getVertex(curveParams[0],curveParams[1])));
                break;

            case SINGLE:

                break;

            case CROSSROAD:
                curveParams = generateCoordinates();
                path.addAll(graph.aStarAlgorithm(graph.getVertex(curveParams[0],curveParams[1]),
                        graph.getVertex(curveParams[2],curveParams[3])));
                break;
        }

        return new Road(path,2,this.space);
    }
    public River generateRiver(){
        return new River(this.generateCurve(),2,this.space);
    }

    private Graph prepareGraph(){
        PerlinNoise2 pNoise = new PerlinNoise2(this.size,35);
        double[][] noiseMatrix = pNoise.getMatrix();
        Graph graph = Graph.graphFromMatrix(noiseMatrix);
        return graph;
    }

    private List<Vertex> generateCurve(){
       Graph graph = prepareGraph();
       int[] curveParams = generateCoordinates();
       List<Vertex> path = graph.aStarAlgorithm(graph.getVertex(curveParams[0],curveParams[1]),
                                                graph.getVertex(curveParams[2],curveParams[3]));

        return path;
    }

    private int[] generateCoordinates(){
        int[] first = generateCoordinate();
        int[] second = generateCoordinate();
        while(first[0] == second[0] || first[1] == second[1])
            second = generateCoordinate();
        return new int[]{first[0], first[1], second[0], second[1]};
    }

    private int[] generateCoordinate(){
        int[] params = new int[2];
        Random random = new Random();
        int first  = random.nextInt(2);
        if(random.nextInt(2) == 0)
            params[first] = 0;
        else params[first] = this.size - 1;
        int temp = -1;
        while (!Utils.between(0,temp,this.size))
            temp = (int) (random.nextGaussian() * (this.size / 2) + (this.size / 2));
        params[(first + 1) % 2] = temp;

        return params;
    }
}
