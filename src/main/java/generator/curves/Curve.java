package generator.curves;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import generator.SpaceMatrix;
import generator.curves.graphs.Vertex;

public abstract class Curve {
    private final static double SCALE = 1;
    private final static int POINTS_PER_POLYNOMIAL = 4;
    private List<Polynomial> polynomials = new LinkedList<Polynomial>();
    protected List<Bridge> bridges = new LinkedList<Bridge>();
    protected int width;

    public Curve(List<Vertex> path, int width, SpaceMatrix spaceMatrix) {
        if(spaceMatrix==null){
            System.out.println("asasasa Curve null space");
        }
        this.width = width;
        List<Vertex> reducedList = reducePoints(path);

        int numberOfPoint = 0;
        int pathSizeDecreased = reducedList.size() - 1;

        while (numberOfPoint < pathSizeDecreased) {
            numberOfPoint = appendPolynomial(reducedList, numberOfPoint);
        }

        for (Polynomial polynomial : polynomials) {
            occupyPlace(polynomial, spaceMatrix);
        }

    }

    protected void occupyPlace(Polynomial polynomial, SpaceMatrix spaceMatrix) {
        if (polynomial.isXDomained) {
            for (int arg = polynomial.start; arg < polynomial.end; arg += 1) {
                int val1 = (int) polynomial.getValueAt(arg);
                int val2 = (int) polynomial.getValueAt(arg + 1);
                int bound1;
                int bound2;
                if (val1 > val2) {
                    bound1 = (int) Math.ceil(width / 2.0);
                    bound2 = -(int) Math.ceil(width / 2.0);
                } else {
                    bound2 = (int) Math.ceil(width / 2.0);
                    bound1 = -(int) Math.ceil(width / 2.0);
                }
                occupyRectangle(arg - (int) Math.ceil(width / 2.0), val1 + bound1, arg + (int) Math.ceil(width / 2.0)
                        + 1, val2 +bound2, spaceMatrix);
            }

            if (polynomial.end - polynomial.start % 2 == 1) {
                int val = (int) polynomial.getValueAt(polynomial.end);
                occupyRectangle(polynomial.end - (int) Math.ceil(width / 2.0), val - (int) Math.ceil(width / 2.0),
                        polynomial.end + (int) Math.ceil(width / 2.0), val + (int) Math.ceil(width / 2.0), spaceMatrix);
            }
        } else {
            for (int arg = polynomial.start; arg < polynomial.end; arg += 1) {
                int val1 = (int) polynomial.getValueAt(arg);
                int val2 = (int) polynomial.getValueAt(arg + 1);
                int bound1;
                int bound2;
                if (val1 > val2) {
                    bound1 = (int) Math.ceil(width / 2.0);
                    bound2 = -(int) Math.ceil(width / 2.0);
                } else {
                    bound2 = (int) Math.ceil(width / 2.0);
                    bound1 = -(int) Math.ceil(width / 2.0);
                }
                occupyRectangle( val1 + bound1, arg - (int) Math.ceil(width / 2.0),
                        val2 + bound2, arg + (int) Math.ceil(width / 2.0) + 1, spaceMatrix);
            }

            if ((polynomial.end - (polynomial.start % 2)) == 0) {
                int val = (int) polynomial.getValueAt(polynomial.end);
                occupyRectangle(val - (int) Math.ceil(width / 2.0),
                        polynomial.end - (int) Math.ceil(width / 2.0), val + (int) Math.ceil(width / 2.0),
                        polynomial.end + (int) Math.ceil( width / 2.0), spaceMatrix);
            }
        }
    }


    protected abstract void occupyRectangle(int xStart, int yStart, int xEnd, int yEnd,
                                            SpaceMatrix spaceMatrix);

    private List<Vertex> reducePoints(List<Vertex> path) {
        int step = (int) (width / SCALE);

        List<Vertex> reducedList;
        if (path.size() % step == 1) {
            reducedList = new ArrayList(path.size() / step);
            for (int i = 0; i < path.size(); i += step) {
                reducedList.add(path.get(i));
            }
        } else {
            reducedList = new ArrayList((path.size()) / step + 1);
            for (int i = 0; i < path.size(); i += step) {
                reducedList.add(path.get(i));
            }

            reducedList.add(path.get(path.size() - 1));
        }

if (reducedList.get(reducedList.size() -1) != (path.get(path.size() - 1))){
    reducedList.add(path.get(path.size() - 1));
            }
        return reducedList;
    }

    private boolean contain(List<Double> args, double val) {
        for (Double d : args) {
            if (d.compareTo( val) == 0) {
                System.out.println("Contain21");
                return true;
            }
        }

        return false;
    }

    private int appendPolynomial(List<Vertex> path, int startIndex) {
        ArrayList<Double> arguments = new ArrayList(POINTS_PER_POLYNOMIAL);
        ArrayList<Double> values = new ArrayList(POINTS_PER_POLYNOMIAL);
        int index = startIndex + 2;
        if (path.get(startIndex).getPos_x() != path.get(startIndex + 1).getPos_x()) {
            //x's will be arguments
            System.out.println("X");
            arguments.add((double) path.get(startIndex).getPos_x());
            values.add((double) path.get(startIndex).getPos_y());

            arguments.add((double) path.get(startIndex + 1).getPos_x());
            values.add((double) path.get(startIndex + 1).getPos_y());

            int counter = 2 ;
            while (counter < POINTS_PER_POLYNOMIAL && path.size() > index) {
                if (!contain(arguments,path.get(index).getPos_x())) {
                    arguments.add((double) path.get(index).getPos_x());
                    values.add((double) path.get(index).getPos_y());
                    counter++;
                    index++;
                } else {
                    break;
                }
            }

            polynomials.add(new Polynomial(true, interpolate(arguments, values),
                    Collections.min(arguments).intValue(), Collections.max(arguments).intValue(), arguments));
        } else {
            //y will be arguments
            System.out.println("Y");
            arguments.add((double) path.get(startIndex).getPos_y());
            values.add((double) path.get(startIndex).getPos_x());

            arguments.add((double) path.get(startIndex + 1).getPos_y());
            values.add((double) path.get(startIndex + 1).getPos_x());

            int counter = 2 ;
            while (counter < POINTS_PER_POLYNOMIAL && path.size() > index) {
                if (!contain(arguments,path.get(index).getPos_y())) {
                    arguments.add((double) path.get(index).getPos_y());
                    values.add((double) path.get(index).getPos_x());
                    counter++;
                    index++;
                } else {
                    break;
                }
            }

            polynomials.add(new Polynomial(false, interpolate(arguments, values),
                    Collections.min(arguments).intValue(), Collections.max(arguments).intValue(), arguments));
        }

        return index - 1;
    }


    private double[] interpolate(List<Double> args, List<Double> values) {
        int n = args.size();
        double[] coeffs = new double[n];

        for (int i = 0; i < n; i++) {
            coeffs[i] = values.get(i);
        }

        for (int i = 0; i < n; i++) {
            for (int j = n - 1; j > i; j--) {
                coeffs[j] = (coeffs[j] - coeffs[j - 1]) / (args.get(j) - args.get(j - i -1));
            }
        }

        return coeffs;
    }

    public class Bridge{
        public final int xMin;
        public final int xMax;
        public final int yMax;
        public final int yMin;
        private String id = "Bridge";

        public Bridge(int xMin, int xMax, int yMin, int yMax) {
            this.xMin = xMin;
            this.xMax = xMax;
            this.yMin = yMin;
            this.yMax = yMax;
        }
    }
    public class Polynomial {
        @JsonProperty("ix_x_domain")public final boolean isXDomained;
        public final int start;
        @JsonProperty("stop")public final int end;
        private double[] coefficients;

        public double getValueAt(double arg) {
            double value = coefficients[coefficients.length - 1];
            for (int i = coefficients.length - 2; i >= 0; i--) {
                value =  value * arg + coefficients[i];
            }

            return value;
        }

        Polynomial(boolean isXDomained, double[] coefficients, int start, int end, List<Double> arguments) {
            this.isXDomained = isXDomained;
            this.coefficients = transformToNewtonForm(coefficients, arguments);
            this.start = start;
            this.end = end;
        }

        private double[] transformToNewtonForm(double[] coef, List<Double> points) {
            double[] coefNewton =  new double[points.size()];
            coefNewton[points.size()  - 1] = coef[points.size()  - 1];
            for (int i = points.size() - 2; i >= 0; i--) {
                coefNewton[i] = coef[i] - points.get(i) * coefNewton[i + 1];
                for (int j = i + 1; j < points.size() - 1; j++) {
                    coefNewton[j] = coefNewton[j] - (points.get(i) * coefNewton[j + 1]);
                }
            }

            return coefNewton;
        }
    }

    public static String getJsonDescription(Curve road) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(road);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }
}
