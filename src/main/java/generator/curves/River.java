package generator.curves;

import java.util.List;
import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import generator.SpaceMatrix;
import generator.curves.graphs.Vertex;

public class River extends Curve {
    private String id = "River";
    private static final double COLISION_TOLERANCE = 0.2;
    private final int MINIMAL_BRIDGE_DIM = width;
    private int bridgeXMax;
    private int bridgeYMax;
    private int bridgeXMin;
    private int bridgeYMin;
    private boolean bridgeStarted = false;

    public River(List<Vertex> path, int width, SpaceMatrix spaceMatrix) {
        super(path, width, spaceMatrix);
        System.out.println(bridges.size() + "bridges num");
        System.out.println(spaceMatrix.toString());
    }

    public void createBridgeIfBig(SpaceMatrix spaceMatrix) {
        if (bridgeXMax - bridgeXMin > MINIMAL_BRIDGE_DIM &&
                bridgeYMax - bridgeYMin > MINIMAL_BRIDGE_DIM) {
                  if (bridges == null) {
                    bridges = new LinkedList();
                  }
                bridges.add(new Bridge(bridgeXMin, bridgeXMax, bridgeYMin,
                        bridgeYMax));
		for(int i = bridgeXMin; i <= bridgeXMax; i++) {
                     for(int j = bridgeYMin; j <= bridgeYMax; j++) {
                   spaceMatrix.setPlace(i, j, SpaceMatrix.PlaceType.Bridge);
                     }
                }
        }
    }

    protected void occupyRectangle(int xStart, int yStart, int xEnd, int yEnd,
                                   SpaceMatrix spaceMatrix) {
        xStart = Math.max(0, xStart);
        yStart = Math.max(0, yStart);
        xEnd = Math.min(spaceMatrix.width - 1, xEnd);
        yEnd = Math.min(spaceMatrix.height - 1, yEnd);

        int colisionCounter = 0;
        for (int i = xStart; i <= xEnd; i++) {
            for (int j = yStart; j <= yEnd; j++) {
                if (spaceMatrix.getPlace(i,j) == SpaceMatrix.PlaceType.Free) {
                    spaceMatrix.setPlace(i, j, SpaceMatrix.PlaceType.River);
                } else {
                  if (spaceMatrix.getPlace(i,j) == SpaceMatrix.PlaceType.Road) {
                      //System.out.println("colli");
                      spaceMatrix.setPlace(i, j, SpaceMatrix.PlaceType.Colision);
                      colisionCounter++;
                  }
                }
            }
        }

        if (colisionCounter >0 && colisionCounter > COLISION_TOLERANCE *
                Math.abs(xStart -xEnd) *  Math.abs(xStart -xEnd)) {
             if (!bridgeStarted) {
                   bridgeStarted = true;
                   if (xStart > xEnd) {
                      bridgeXMax = xStart;
                      bridgeXMin = xEnd;
                   } else {
                     bridgeXMax = xEnd;
                     bridgeXMin = xStart;
                   }

                   if (yStart > yEnd) {
                      bridgeYMax = yStart;
                      bridgeYMin = yEnd;
                   } else {
                     bridgeYMax = yEnd;
                     bridgeYMin = yStart;
                   }
             } else {
                 processBridgeX(xStart, xEnd);
                 processBridgeY(yStart, yEnd);
             }

        } else {
            if (bridgeStarted) {
                createBridgeIfBig(spaceMatrix);
                bridgeStarted =  false;
            }
        }
    }

    private void processBridgeX(int xStart, int xEnd) {
      if (xStart > xEnd) {
          if (xStart > bridgeXMax) {
            bridgeXMax = xStart;
          }

          if (xEnd > bridgeXMin) {
            bridgeXMin = xEnd;
          }
      } else {
        if (xEnd > bridgeXMax) {
          bridgeXMax = xEnd;
        }

        if (xStart > bridgeXMin) {
          bridgeXMin = xStart;
        }
      }
    }

    private void processBridgeY(int xStart, int xEnd) {
      if (xStart > xEnd) {
          if (xStart > bridgeYMax) {
            bridgeYMax = xStart;
          }

          if (xEnd > bridgeYMin) {
            bridgeYMin = xEnd;
          }
      } else {
        if (xEnd > bridgeYMax) {
          bridgeYMax = xEnd;
        }

        if (xStart > bridgeYMin) {
          bridgeYMin = xStart;
        }
      }
    }

    public static String getJsonDescription(River road) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(road);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }
}
