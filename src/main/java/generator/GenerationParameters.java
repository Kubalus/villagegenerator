package generator;

import java.util.Map;

public class GenerationParameters {
    boolean river;
    int size;
    int wealth;
    RoadType roadType;

    public enum RoadType {
        CROSSROAD, FORKED, SINGLE;
    }

    public GenerationParameters(){

    }

    public GenerationParameters(Map<String, String> params){
        if(params.get("river").compareTo("yes") == 0){
            System.out.println("river  yes");
            river = true;
        }else {
            river =false;
        }
        System.out.println(params.get(("road")).toString());
        if (params.get("road").compareTo("Single") == 0)
            roadType = RoadType.SINGLE;
        else{
            if (params.get("road").compareTo("Forked") == 0)
                roadType = RoadType.FORKED;
            else {
                if (params.get("road").compareTo("Crossroads") == 0)
                    roadType = RoadType.CROSSROAD;
            }
        }

        size = 5 * Integer.parseInt(params.get("size_range"));
        System.out.println(size);
        wealth = Integer.parseInt(params.get("wealth_range"));
        System.out.println(wealth);
    }
}
