package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;


public class VillagerGenerator {
    private Random random = new Random();

    private String[] poorProffesions = {
            "Baker", "Bricklayer", "Candlemaker", "Carpenter",
            "Cook", "Farmer", "Fisherman", "Forester",
            "Furrier", "Gardener", "Gravedigger", "Herbalist",
            "Hunter", "Leatherworker", "Locksmith", "Minstrel",
            "Peddler", "Potter", "Rat Catcher", "Servant",
            "Shoemaker", "Weaver"
    };

    private String[] richProffesions = {
            "Apothecarist", "Architect", "Armorer", "Artist",
            "Astrologer", "Barrister", "Bookbinder", "Bowyer",
            "Brewer", "Cartographer", "Clothier", "Dyer",
            "Engineer", "Engraver", "Fortune-Teller", "Glassblower",
            "Grain Merchant", "Innkeeper", "Jeweler", "Miner",
            "Moneylender", "Painter", "Physician", "Playwright",
            "Sailor", "Scribe", "Stonecarver", "Storyteller",
    };

    public Villager[] getVillagers(int count, int wealthLevel) {
        Villager[] villagers = new Villager[count];
        try {
            URL url = new URL("https://uinames.com/api/?region=germany&amount=" + Integer.toString(count));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = in.readLine();
            System.err.println(line);
            JSONArray response = new JSONArray(line);
            for (int i = 0; i < count; i++) {
                JSONObject obj = response.getJSONObject(i);
                villagers[i] = new Villager();
                villagers[i].name = obj.getString("name");
                villagers[i].surname = obj.getString("surname");
                villagers[i].age = getAge();
                villagers[i].proffesion = getProffrsion(wealthLevel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return villagers;
    }


    public String getProffrsion(int wealthLevel) {

        int probability = (random.nextInt(10) + 1) % 10;    //1-10
        String proffesion;

        switch (wealthLevel) {
            case 1:        //$	  ->  70%-30%
                if (probability <= 7) {
                    proffesion = poorProffesions[random.nextInt(poorProffesions.length)];
                } else {
                    proffesion = richProffesions[random.nextInt(richProffesions.length)];
                }
                break;

            case 2:        //$$ -> 50%-50%
                if (probability <= 5) {
                    proffesion = poorProffesions[random.nextInt(poorProffesions.length)];
                } else {
                    proffesion = richProffesions[random.nextInt(richProffesions.length)];
                }
                break;

            case 3:        //$$$  -> 30%-70%
                if (probability <= 3) {
                    proffesion = poorProffesions[random.nextInt(poorProffesions.length)];
                } else {
                    proffesion = richProffesions[random.nextInt(richProffesions.length)];
                }
                break;

            default:
                proffesion = "Farmer";
        }
        return proffesion;
    }


    public int getAge() {
        int age;
        do {
            age = (int) (random.nextGaussian() * 25 + 2);
        } while (age <= 0 || age > 80);

        return age;
    }

}