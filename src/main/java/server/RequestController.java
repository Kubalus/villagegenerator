package server;

import generator.GenerationParameters;
import generator.MainGenerator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class RequestController {

    @RequestMapping("/map")
    public String map(@RequestParam Map<String,String> allParams) {
        for(String name : allParams.keySet()) {
            System.out.println(name);
        }

        GenerationParameters param = new GenerationParameters(allParams);
        MainGenerator.Village  village = new MainGenerator().generateVillage(param);
        System.out.println(village.getJsonDescription() + "req");
        return village.getJsonDescription();
	}
	
	@RequestMapping("/pdf")
    public String pdf() {
        return "PDF will be sent here *-*";
    }
}