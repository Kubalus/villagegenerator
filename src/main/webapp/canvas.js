const scala = 5;

var canvas = document.getElementById('drawing'),
    c = canvas.getContext('2d'),
    prec = 1,
    data, dir, line,
    d,		//domain
    a,b,	//start and finish of fragment
    cf,	//array of coefficients
    i, j,	//current coordinates
    m, n;
c.lineWidth = 10;

function draw(data){
    dir = JSON.parse(data);

    for(m in dir){
        if (dir[m].id.localeCompare("Road") == 0) {
          line = dir[m].polynomials;
          c.strokeStyle = "black"
          for (n in line){
              drawCurveFragment(line[n].ix_x_domain, line[n].start, line[n].stop, line[n].coefficients);
          }
        }

        if (dir[m].id.localeCompare("River") == 0) {
          line = dir[m].polynomials;

          c.strokeStyle = "blue"
          for (n in line){
              drawCurveFragment(line[n].ix_x_domain, line[n].start, line[n].stop, line[n].coefficients);
          }
          lineB = dir[m].bridges
          for (bridge in lineB){
            drawBridge(lineB[bridge])
          }
        }

    } //test

}

function drawBridge(bridge){
  c.fillStyle = "brown";
  widthBridge = (bridge.xMax - bridge.xMin) * scala;
  heightBridge = (bridge.yMax - bridge.yMin) * scala
  c.fillRect(bridge.xMin * scala - widthBridge/2, bridge.yMin *scala - heightBridge/2,
    widthBridge * 2, heightBridge * 2);
}

function drawCurveFragment(d, a, b, cf){
    var first = true;
    //przeskalowanie do canvasu
    //a=a*scala;
    //b=b*scala;
    prec = (b-a)/canvas.width;
    c.beginPath();
    for(i=a; i<=b; i+=prec){
        j=0;
        for(var n=0; n<cf.length; n++){
            j=j+cf[n]*Math.pow(i, (n));
        }

        console.log(i);
        console.log(j);

        if(d){
            if(first) {
                c.moveTo(i*scala,j*scala);
                first = false;
            }
            else c.lineTo(i*scala,j*scala);
        }else{
            if(first) {
                c.moveTo(j*scala,i*scala);
                first = false;
            }
            else c.lineTo(j*scala,i*scala);
        }
    }
    c.stroke();

}

function drawHouse(i, j){
    c.beginPath();
    c.arc(i, j, 3, 0, 2 * Math.PI);
    c.fill();
}

function clear(){
    c.clearRect(0, 0, canvas.width, canvas.height);
}
